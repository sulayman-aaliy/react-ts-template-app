import React from 'react';
import logo from './logo.svg';
import './App.css';
import GlobalActions from "./components/GlobalActions/global-actions";
import * as lib from "./common/lib";
import AutoDropComplete from './components/AutoDropComplete/AutoDropComplete';
import Home from './Home';
import Stock from './Stock';
import Contact from './Contact';
import { useRef, useState } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";

export enum SectionState {
  initializing = 0,
  outOfDate = 1,
  upToDate = 2,
  toUpdate = 3
};

export const SectionStateContext = React.createContext(SectionState.upToDate);

function App() {
  
  const [sectionState, setSectionStateFilter] = useState<SectionState>(SectionState.initializing);

  function updateSectionState(s: SectionState) {
    setSectionStateFilter(s);
  }

  function refresh() {
    setSectionStateFilter(SectionState.outOfDate);
  }

  function save() {
    setSectionStateFilter(SectionState.toUpdate);
  }

  return <HashRouter>
    <div className="App">
      <header className="App-header">
       <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> 
      </header>
    </div>
    <ul className="nav-links">
      <li><NavLink exact to="/">Home</NavLink></li>
      <li><NavLink to="/stock">Stock</NavLink></li>
      <li><NavLink to="/contact">Contact</NavLink></li>
    </ul>
    <div>
      <SectionStateContext.Provider value={sectionState}>
        <div className="app-container">
          <div className="top-area">
            <button onClick={refresh}>refresh</button>
            <button onClick={save}>save</button>
          </div>
          <Route exact path="/" render={(props) => <Home {...props} title={`WELCOME HOME`} />} />
          <Route path="/stock" render={(props) => <Stock {...props} stateUpdateCallback={updateSectionState} />} />
          <Route path="/contact" component={Contact} />
        </div>
      </SectionStateContext.Provider>
    </div>
  </HashRouter>;
}

export default App;
