
export interface AppContext {
    content: string;
}

export interface AppActionSideEffect {
    callback: () => Promise<any>;
    data: any;
}

export enum AppActionStep {
    RQ = "requested",
    RN = "running",
    DN = "done",
    AB = "aborted",
    N = ""
}

export interface AppAction {
    code: string;
    label?: string;
    callback?: () => Promise<AppActionSideEffect>;
    step: AppActionStep;
    message?: string | null;
}

export const defaultAppAction: AppAction = { code: "", step: AppActionStep.N };

export function GetDefaultAction(action: AppAction, context: AppContext): (() => Promise<any>) | null {
    if (action.code === "refresh") {
        if (context.content === "home") {
            
        }
    }
    return null;
}
