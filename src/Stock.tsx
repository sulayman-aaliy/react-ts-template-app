import * as React from "react";
import { useRef, useState, useEffect, useCallback, useReducer, ReducerAction, useContext } from "react";
import "./global.css";
import GlobalActions from "./components/GlobalActions/global-actions";
import ReadableGrid, { GridDimension, GridDataType, GridPagingStyle, GridColumn } from "./components/ReadableGrid/readable-grid";
import * as lib from "./common/lib";
import { SectionStateContext, SectionState } from "./App";

interface StockSearchFilter {
    broadTerm?: string;
    itemName?: string;
    keyword?: string;
    category?: string;
    minQuantity?: number;
}

interface StockSearchResult {
    item: StockItem;
    actions: (() => void)[];
}

interface SourceItem {
    keycode: string;
    codes: string[];
    name: string;
    keywords: string[];
    category: string[];
}

interface StockItem {
    items: SourceItem[];
    keywords: string[];
    quantity: number;
}

interface StockNewQuantity {
    keycode: string;
    current: number;
    adjustment: number;
}

enum StockContent {
    Home, Search, Add, Clear
}

interface StockProps { stateUpdateCallback: (s: SectionState) => void }

export default function Stock(props: StockProps) {
    const pageState = useContext(SectionStateContext);

    const [searchFilter, setFilter] = useState<StockSearchFilter>();
    const [newItems, setNewItems] = useState<StockItem[]>([]);
    const [newQuantities, setNewQuantities] = useState<StockNewQuantity[]>([]);
    const [contentFocus, setContentFocus] = useState(StockContent.Home);
    const [lastStock, setLastStock] = useState<StockSearchResult[]>([]);
    const [isLoading, setIsLoading] = useState(true);

    const searchResults = useRef<StockSearchResult[]>([]);
    const lastStock1 = useRef<StockSearchResult[]>([]);
    const stateUpdate = useRef<((s: SectionState) => void)>(props.stateUpdateCallback);

    function defaultColumns(): GridColumn[] {
        return [
            { code: "keycode", label: "Code", showLabel: true, wrapLabel: false, wrapValue: false, type: GridDataType.str, hidden: false, sortable: false },
            { code: "name", label: "Désignation", showLabel: true, wrapLabel: false, wrapValue: false, type: GridDataType.str, hidden: false, sortable: false },
            { code: "quantity", label: "Stock", showLabel: true, wrapLabel: false, wrapValue: false, type: GridDataType.int, hidden: false, sortable: false }
        ];
    }

    function getLastStock() {
        lastStock1.current = [
            {
                item: { items: [{ keycode: "tnq1", codes: ["tnq1"], name: "Tunique", keywords: ["Tunique"], category: ["PAP"] }], keywords: ["tunique", "robe", "chemise"], quantity: 1 },
                actions: []
            }
        ];
        setLastStock([
            {
                item: { items: [{ keycode: "tnq1", codes: ["tnq1"], name: "Tunique", keywords: ["Tunique"], category: ["PAP"] }], keywords: ["tunique", "robe", "chemise"], quantity: 1 },
                actions: []
            }
        ]);
    }

    const renderReadableGrid = useCallback(() => {
        console.log("renderReadableGrid");
        return <ReadableGrid pagingStyle={GridPagingStyle.noPage} rowHeightStyle={GridDimension.auto} dataSource={lastStock} columns={defaultColumns()} />;
    }, [lastStock]);

    const renderMainArea = useCallback(() => {
        console.log("render Main Area");
        if (isLoading) {
            console.log("render is loading");
            return <span>Reloading...</span>;
        }
        else {
            console.log("render up to date");
            return <>
                {/* liste derniers stock */}
                {contentFocus === StockContent.Home && renderReadableGrid()}

                {/* chercher stock - resultats stock - modifier stock */}

                {/* ajouter stock */}

                {/* vider stock */}
            </>;
        
        }
    }, [isLoading, contentFocus]);

    useEffect(() => {
        if (pageState !== SectionState.upToDate) {
            // perform update or reload ...
            if (pageState === SectionState.initializing) {
                getLastStock();
            }
            setTimeout(() => {
                stateUpdate.current(SectionState.upToDate);
            }, 2000);
            setIsLoading(true);
        }
        else {
            setIsLoading(false);
        }
    });

    return <>
        <div className="left-area">
            <ul>
                <li className={contentFocus === StockContent.Home ? "active" : ""}>Derniers stocks</li>
                <li className={contentFocus === StockContent.Search ? "active" : ""}>Recherche</li>
                <li className={contentFocus === StockContent.Add ? "active" : ""}>Ajouter du stock</li>
                <li className={contentFocus === StockContent.Clear ? "active" : ""}>Vider des stocks</li>
            </ul>
        </div>
        <div className="main-area">
            {renderMainArea()}
        </div>
        <div className="bottom-area"></div>
    </>;
}

