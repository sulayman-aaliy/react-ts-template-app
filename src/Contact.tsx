import * as React from "react";
import { useRef, useState, useEffect, useCallback, useReducer, ReducerAction } from "react";
import "./global.css";

//const context: lib.AppContext = { content: 'contact' };
//const ContactContext = React.createContext(context);

interface ContactProps {}

export default function Contact(props: ContactProps) {
    
    return <>
        <div className="left-area"></div>
        <div className="main-area">Contact</div>
        <div className="bottom-area">@...</div>
    </>;
}
