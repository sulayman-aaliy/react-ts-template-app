import * as React from "react";
import * as lib from "../../common/lib";
import { useRef, useState, useEffect, useCallback, useReducer, useContext } from "react";

export interface GlobalActionsProps {
    initAction?: lib.AppAction;
    saveAction?: lib.AppAction;
    cancelAction?: lib.AppAction;
    refreshAction?: lib.AppAction;
}

// export interface GlobalActionsState {
//     isReadonly: boolean;
//     currentAction?: lib.AppAction;
//     actionStack: lib.AppAction[] | null;
// }

// export interface GlobalAction {
//     context: lib.AppContext;
//     before?: () => Promise<any>;
//     after?: () => any;
// }

// export const CANCEL_REQUESTED = "cancel-requested";
// export interface CancelAction extends GlobalAction {
//     type: typeof CANCEL_REQUESTED;
// }

// export const SAVE_REQUESTED = "save-requested";
// export interface SaveAction extends GlobalAction {
//     type: typeof SAVE_REQUESTED;
// }

// export const REFRESH_REQUESTED = "refresh-requested";
// export interface RefreshAction extends GlobalAction {
//     type: typeof REFRESH_REQUESTED;
// }

// const defaultState: GlobalActionsState = { isReadonly: true, actionStack: [] };
// function reducer(state: GlobalActionsState, action: SaveAction | CancelAction | RefreshAction): GlobalActionsState {
//     let s = defaultState;
//     Object.assign(s, state);
//     if (s.actionStack === null) {
//         s.actionStack = [];
//     }
//     if (state.currentAction) {
//         s.actionStack.push(state.currentAction);
//     }
//     s.currentAction = { step: "requesting", code: action.type.split('-')[0] };
//     const callback = lib.GetDefaultAction(s.currentAction, action.context);
//     if (callback === null) {
//         return state;
//     }
//     s.currentAction.callback = async e => {
//         e.callback = callback;
//         if (action.before) {
//             const b = await action.before();
//         }
//         const refresh = await e.callback();
//         return action.after ? action.after() : true;
//     };
    
//     return s;
// }

function GlobalActions(props: GlobalActionsProps) {
    const [isReadonly, setReadonly] = useState(true);
    const [currentAction, setAction] = useState(lib.defaultAppAction);
    //const [current]
    const [actionStack, backupAction] = useState<lib.AppAction[]>([]);

    const initAction = useRef(props.initAction ? props.initAction : lib.defaultAppAction);
    const saveAction = useRef(props.saveAction ? props.saveAction : lib.defaultAppAction);
    const cancelAction = useRef(props.cancelAction ? props.cancelAction : lib.defaultAppAction);
    const refreshAction = useRef(props.refreshAction ? props.refreshAction : lib.defaultAppAction);

    function save(event: React.MouseEvent) {
        setAction({ ...saveAction.current, step: lib.AppActionStep.RQ });
    }

    function cancel(event: React.MouseEvent) {
        setAction({ ...cancelAction.current, step: lib.AppActionStep.RQ });
    }

    function refresh(event: React.MouseEvent) {
        console.log(`${refreshAction.current.code} RQ`);
        setAction({ ...refreshAction.current, step: lib.AppActionStep.RQ });
    }

    useEffect(() => {
        if (initAction.current !== null && initAction.current.callback && initAction.current.step === lib.AppActionStep.RQ) {
            initAction.current.callback()
                .then(x => {
                    initAction.current.step = lib.AppActionStep.DN;
                    x.callback();
                })
                .catch(r => {
                    console.error(r);
                    initAction.current.step = lib.AppActionStep.AB;
                });
        }
        if (currentAction.step === lib.AppActionStep.RQ) {
            if (!currentAction.callback) {
                setAction(lib.defaultAppAction);
            }
            else {
                console.log(`${currentAction.code} RN`);
                setAction({ ...currentAction, step: lib.AppActionStep.RN });
            }
        }
        else if (currentAction.step === lib.AppActionStep.RN && currentAction.callback) {
            console.log(`${currentAction.code} starting`);
            currentAction.callback().then(e => {
                backupAction(actionStack.concat({ ...currentAction, step: lib.AppActionStep.DN }));
                setAction({ ...currentAction, step: lib.AppActionStep.DN });
                e.callback();
            }).catch(r => {
                console.error(r);
                backupAction(actionStack.concat({ ...currentAction, step: lib.AppActionStep.AB, message: r }));
                setAction({ ...currentAction, step: lib.AppActionStep.AB });
            });
        }
    });

    return <>
        
        <button onClick={cancel}>{cancelAction.current.label || "cancel"}</button>
        <button onClick={save}>{saveAction.current.label || "save"}</button>
    </>;
}

export default GlobalActions;