
import * as React from "react";
import { useRef, useCallback } from "react";
//import { NumericDictionary } from "lodash";
import "./readable-grid.css";

export enum GridDimension {
    number,
    auto,
    min
}

export enum GridDataType {
    int = "INTEGER",
    dec = "DECIMAL",
    str = "STRING",
    dat = "DATE",
    any = "ANY"
}

export enum GridVisibility {
    normal,
    alwaysVisible,
    alwaysHidden,
    noWidth,
    doNotRender
}

export enum GridPagingStyle {
    noPage,
    max5,
    unlimited
}

export interface GridColumn {
    code: string;
    label: string;
    showLabel: boolean;
    wrapLabel: boolean;
    wrapValue: boolean;
    widthStyle?: GridDimension;
    width?: number;
    type: GridDataType;
    visibility?: GridVisibility;
    hidden: boolean;
    sortable: boolean;
    renderer?: () => JSX.Element;
    rowNames?: string[];
}

export interface GridProps {
    columns: GridColumn[];
    rowHeightStyle: GridDimension;
    rowHeight?: number;
    headRenderer?: (c: GridColumn) => JSX.Element;
    cellRenderer?: (c: GridColumn, value: any) => JSX.Element;
    nbMaxVisibleRows?: number;
    pagingStyle: GridPagingStyle;
    dataSource: any[];
}

export default function ReadableGrid(props: GridProps) {
    const columns = useRef(props.columns);
    const rows = useRef(props.dataSource);
    const showHeaders = useCallback(() => rows.current.length > 0 && columns.current.some(c => c.showLabel), [columns, rows]);
    const renderHeader = useCallback(() => {
        let x = 0;
        return columns.current.map(c => {
            if (c.visibility === GridVisibility.doNotRender) {
                return <></>;
            }
            x++;
            if (c.renderer) {
                return c.renderer.bind(c)();
            }
            if (props.headRenderer) {
                return props.headRenderer(c);
            }
            return renderCell(c, x, 1);
        });
    }, [columns]);

    const renderRows = useCallback(() => {
        let x = 0;
        return rows.current.map((row, index) => {
            return columns.current.map(c => {
                if (c.visibility === GridVisibility.doNotRender) {
                    return <></>;
                }
                x++;
                return renderCell(c, x, index + 2, row);
            });
        });
    }, [rows, columns]);

    function renderCell(c: GridColumn, x: number, row: number, obj?: any): JSX.Element {
        function getValue() {
            if (obj) {
                if (obj[c.code] !== undefined) {
                    return obj[c.code];
                }
                let item = obj.item || obj;
                if (item[c.code] !== undefined) {
                    return item[c.code];
                }
                let key = Object.keys(item).filter(k => item[k][c.code] !== undefined)[0];
                if (key) {
                    return item[key][c.code];
                }
                key = Object.keys(item).filter(k => Array.isArray(item[k]) && item[k].some((y: any) => y[c.code] !== undefined))[0];
                if (key) {
                    return item[key].filter((y: any) => y[c.code] !== undefined)[0][c.code];
                }
                return "";
            }
            else {
                return c.showLabel ? c.label : "";
            }
        }
        
        let isRenderingColumn: boolean = obj == null;
        let hiddingClass = c.visibility === GridVisibility.alwaysVisible ? "" : c.visibility === GridVisibility.alwaysHidden || c.hidden ? " hidden" : c.visibility === GridVisibility.noWidth ? " squashed" : "";
        let wrappingClass = c.wrapLabel ? " inline-text" : "";
        let widthClass = c.visibility === GridVisibility.noWidth ? " no-width-col" : c.widthStyle === GridDimension.auto ? " auto-width-col" : null;
        if (widthClass !== null) {
            return <div key={`${x}-${row}`} className={`${isRenderingColumn ? "column" : ""}${hiddingClass + wrappingClass + widthClass}`} style={{gridRow: row, gridColumn: x}}>
                {getValue()}
            </div>;
        }
        //let widthGrid = c.widthStyle === GridDimension.min ? `minmax(${c.width}px, 1fr)` : `minmax(${c.width}px, ${c.width}px)`;
        
        let styles: React.CSSProperties = {gridRow: row, gridColumn: x};
        if (c.widthStyle === GridDimension.min) {
            styles.minWidth = `${c.width}px`;
        }
        else {
            styles.minWidth = `${c.width}px`;
            styles.maxWidth = `${c.width}px`;
        }
        return <div key={`${x}-${row}`} className={`${isRenderingColumn ? "column" : ""}${hiddingClass + wrappingClass}`} style={styles}>
            {getValue()}
        </div>;
    }

    return <div className="react-grid">
        {showHeaders() && renderHeader()}
        {rows.current.length > 0 && renderRows()}
    </div>;
}
