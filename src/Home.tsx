import * as React from "react";
import { useRef, useState, useEffect, useCallback, useReducer, ReducerAction } from "react";
import GlobalActions from "./components/GlobalActions/global-actions";
import * as lib from "./common/lib";
import "./global.css";

interface HomeProps { title: string }

export default function Home(props: HomeProps) {
    
    return <>
        <div className="left-area"></div>
        <div className="main-area">{props.title}</div>
        <div className="bottom-area"></div>
    </>;
}

